package com.aubrun.eric.evaluationHAS.business.mapper;

import com.aubrun.eric.evaluationHAS.business.dto.SearchCustomerDto;
import com.aubrun.eric.evaluationHAS.model.SearchCustomer;
import com.aubrun.eric.evaluationHAS.model.annotations.ExcludeClassFromJacocoGeneratedReport;

@ExcludeClassFromJacocoGeneratedReport
public class SearchCustomerDtoMapper {

    static public SearchCustomerDto toDto(SearchCustomer searchCustomer){

        SearchCustomerDto dto = new SearchCustomerDto();
        dto.setCustomerId(searchCustomer.getCustomerId());
        dto.setEvaluationHASUser(searchCustomer.getEvaluationHASUser());
        dto.setCustomerFirstName(searchCustomer.getCustomerFirstName());
        dto.setCustomerLastName(searchCustomer.getCustomerLastName());
        dto.setSocialSecurityNumber(searchCustomer.getSocialSecurityNumber());
        dto.setMutualName(searchCustomer.getMutualName());
        dto.setAge(searchCustomer.getAge());
        dto.setDateBirth(searchCustomer.getDateBirth());
        dto.setEntryDate(searchCustomer.getEntryDate());
        return dto;
    }

    static public SearchCustomer toEntity(SearchCustomerDto searchCustomerDto){

        SearchCustomer entity = new SearchCustomer();
        entity.setCustomerId(searchCustomerDto.getCustomerId());
        entity.setEvaluationHASUser(searchCustomerDto.getEvaluationHASUser());
        entity.setCustomerFirstName(searchCustomerDto.getCustomerFirstName());
        entity.setCustomerLastName(searchCustomerDto.getCustomerLastName());
        entity.setSocialSecurityNumber(searchCustomerDto.getSocialSecurityNumber());
        entity.setMutualName(searchCustomerDto.getMutualName());
        entity.setAge(searchCustomerDto.getAge());
        entity.setDateBirth(searchCustomerDto.getDateBirth());
        entity.setEntryDate(searchCustomerDto.getEntryDate());
        return entity;
    }
}
