package com.aubrun.eric.evaluationHAS.business.service.exception;

import com.aubrun.eric.evaluationHAS.model.annotations.ExcludeClassFromJacocoGeneratedReport;

@ExcludeClassFromJacocoGeneratedReport
public class WrongIdentifier extends RuntimeException {

    public WrongIdentifier() {
        super("Please verify your login details");
    }


}
