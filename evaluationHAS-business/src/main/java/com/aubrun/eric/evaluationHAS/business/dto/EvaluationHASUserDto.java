package com.aubrun.eric.evaluationHAS.business.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public class EvaluationHASUserDto {

    private Integer evaluationHasUserId;
    private String username;
    private String password;
    private String email;
    private Integer phoneUser;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateBirthUser;
    private Long socialSecurityNumberUser;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime entryDateUser;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime releaseDateUser;
    private Integer ageUser;
    private List<CustomerDto> customerDtoList;
    private Set<RoleDto> roleDtoSet;
    private Set<EstablishmentDto> establishmentDtoSet;

    public Integer getEvaluationHasUserId() {
        return evaluationHasUserId;
    }

    public void setEvaluationHasUserId(Integer evaluationHasUserId) {
        this.evaluationHasUserId = evaluationHasUserId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getPhoneUser() {
        return phoneUser;
    }

    public void setPhoneUser(Integer phoneUser) {
        this.phoneUser = phoneUser;
    }

    public LocalDate getDateBirthUser() {
        return dateBirthUser;
    }

    public void setDateBirthUser(LocalDate dateBirthUser) {
        this.dateBirthUser = dateBirthUser;
    }

    public Long getSocialSecurityNumberUser() {
        return socialSecurityNumberUser;
    }

    public void setSocialSecurityNumberUser(Long socialSecurityNumberUser) {
        this.socialSecurityNumberUser = socialSecurityNumberUser;
    }

    public LocalDateTime getEntryDateUser() {
        return entryDateUser;
    }

    public void setEntryDateUser(LocalDateTime entryDateUser) {
        this.entryDateUser = entryDateUser;
    }

    public LocalDateTime getReleaseDateUser() {
        return releaseDateUser;
    }

    public void setReleaseDateUser(LocalDateTime releaseDateUser) {
        this.releaseDateUser = releaseDateUser;
    }

    public Integer getAgeUser() {
        return ageUser;
    }

    public void setAgeUser(Integer ageUser) {
        this.ageUser = ageUser;
    }

    public List<CustomerDto> getCustomerDtoList() {
        return customerDtoList;
    }

    public void setCustomerDtoList(List<CustomerDto> customerDtoList) {
        this.customerDtoList = customerDtoList;
    }

    public Set<RoleDto> getRoleDtoSet() {
        return roleDtoSet;
    }

    public void setRoleDtoSet(Set<RoleDto> roleDtoSet) {
        this.roleDtoSet = roleDtoSet;
    }

    public Set<EstablishmentDto> getEstablishmentDtoSet() {
        return establishmentDtoSet;
    }

    public void setEstablishmentDtoSet(Set<EstablishmentDto> establishmentDtoSet) {
        this.establishmentDtoSet = establishmentDtoSet;
    }
}
