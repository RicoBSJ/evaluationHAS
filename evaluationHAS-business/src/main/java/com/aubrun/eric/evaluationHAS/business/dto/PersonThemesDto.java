package com.aubrun.eric.evaluationHAS.business.dto;

import com.aubrun.eric.evaluationHAS.model.EPersonThemes;

public class PersonThemesDto {

    private Integer personThemesId;
    private String criteriaPersonName;
    private EPersonThemes personThemesType;

    public Integer getPersonThemesId() {
        return personThemesId;
    }

    public void setPersonThemesId(Integer personThemesId) {
        this.personThemesId = personThemesId;
    }

    public String getCriteriaPersonName() {
        return criteriaPersonName;
    }

    public void setCriteriaPersonName(String criteriaPersonName) {
        this.criteriaPersonName = criteriaPersonName;
    }

    public EPersonThemes getPersonThemesType() {
        return personThemesType;
    }

    public void setPersonThemesType(EPersonThemes personThemesType) {
        this.personThemesType = personThemesType;
    }
}
