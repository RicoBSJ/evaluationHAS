package com.aubrun.eric.evaluationHAS.business.mapper;

import com.aubrun.eric.evaluationHAS.business.dto.EstablishmentDto;
import com.aubrun.eric.evaluationHAS.model.Establishment;

public class EstablishmentDtoMapper {

    static public EstablishmentDto toDto(Establishment establishment) {
        if (establishment == null) return null;
        EstablishmentDto dto = new EstablishmentDto();
        dto.setEstablishmentId(establishment.getEstablishmentId());
        dto.setEstablishmentName(establishment.getEstablishmentName());
        return dto;
    }

    static public Establishment toEntity(EstablishmentDto establishmentDto) {
        Establishment entity = new Establishment();
        entity.setEstablishmentId(establishmentDto.getEstablishmentId());
        entity.setEstablishmentName(establishmentDto.getEstablishmentName());
        return entity;
    }
}
