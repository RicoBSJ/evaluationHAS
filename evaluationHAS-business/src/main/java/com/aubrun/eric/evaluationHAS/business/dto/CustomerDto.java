package com.aubrun.eric.evaluationHAS.business.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public class CustomerDto {

    private Integer customerId;
    private String evaluationHASUser;
    private String customerFirstName;
    private String customerLastName;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateBirth;
    private Long socialSecurityNumber;
    private String mutualName;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime entryDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime releaseDate;
    private Integer age;
    private Set<JuridicProtectionDto> juridicProtectionDtoSet;
    private Set<EstablishmentDto> establishmentDtoSet;
    private List<PersonThemesDto> personThemesDtoList;
    private List<ProfessionalsThemesDto> professionalsThemesDtoList;
    private List<ESMSThemesDto> esmsThemesDtoList;

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getEvaluationHASUser() {
        return evaluationHASUser;
    }

    public void setEvaluationHASUser(String evaluationHASUser) {
        this.evaluationHASUser = evaluationHASUser;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public LocalDate getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(LocalDate dateBirth) {
        this.dateBirth = dateBirth;
    }

    public Long getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(Long socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getMutualName() {
        return mutualName;
    }

    public void setMutualName(String mutualName) {
        this.mutualName = mutualName;
    }

    public LocalDateTime getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(LocalDateTime entryDate) {
        this.entryDate = entryDate;
    }

    public LocalDateTime getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDateTime releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Set<JuridicProtectionDto> getJuridicProtectionDtoSet() {
        return juridicProtectionDtoSet;
    }

    public void setJuridicProtectionDtoSet(Set<JuridicProtectionDto> juridicProtectionDtoSet) {
        this.juridicProtectionDtoSet = juridicProtectionDtoSet;
    }

    public Set<EstablishmentDto> getEstablishmentDtoSet() {
        return establishmentDtoSet;
    }

    public void setEstablishmentDtoSet(Set<EstablishmentDto> establishmentDtoSet) {
        this.establishmentDtoSet = establishmentDtoSet;
    }

    public List<PersonThemesDto> getPersonThemesDtoList() {
        return personThemesDtoList;
    }

    public void setPersonThemesDtoList(List<PersonThemesDto> personThemesDtoList) {
        this.personThemesDtoList = personThemesDtoList;
    }

    public List<ProfessionalsThemesDto> getProfessionalsThemesDtoList() {
        return professionalsThemesDtoList;
    }

    public void setProfessionalsThemesDtoList(List<ProfessionalsThemesDto> professionalsThemesDtoList) {
        this.professionalsThemesDtoList = professionalsThemesDtoList;
    }

    public List<ESMSThemesDto> getEsmsThemesDtoList() {
        return esmsThemesDtoList;
    }

    public void setEsmsThemesDtoList(List<ESMSThemesDto> esmsThemesDtoList) {
        this.esmsThemesDtoList = esmsThemesDtoList;
    }
}
