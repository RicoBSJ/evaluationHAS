package com.aubrun.eric.evaluationHAS.business.mapper;

import com.aubrun.eric.evaluationHAS.business.dto.JuridicProtectionDto;
import com.aubrun.eric.evaluationHAS.model.JuridicProtection;

public class JuridicProtectionDtoMapper {

    static public JuridicProtectionDto toDto(JuridicProtection juridicProtection) {
        if (juridicProtection == null) return null;
        JuridicProtectionDto dto = new JuridicProtectionDto();
        dto.setJuridicProtectionId(juridicProtection.getJuridicProtectionId());
        dto.setJuridicProtectionName(juridicProtection.getJuridicProtectionName());
        return dto;
    }

    static public JuridicProtection toEntity(JuridicProtectionDto juridicProtectionDto) {
        JuridicProtection entity = new JuridicProtection();
        entity.setJuridicProtectionId(juridicProtectionDto.getJuridicProtectionId());
        entity.setJuridicProtectionName(juridicProtectionDto.getJuridicProtectionName());
        return entity;
    }
}
