package com.aubrun.eric.evaluationHAS.business.service;

import com.aubrun.eric.evaluationHAS.business.dto.EvaluationHASUserDto;
import com.aubrun.eric.evaluationHAS.business.mapper.EvaluationHASUserDtoMapper;
import com.aubrun.eric.evaluationHAS.consumer.repository.CustomerRepository;
import com.aubrun.eric.evaluationHAS.consumer.repository.EstablishmentRepository;
import com.aubrun.eric.evaluationHAS.consumer.repository.EvaluationHasUserRepository;
import com.aubrun.eric.evaluationHAS.consumer.repository.RoleRepository;
import com.aubrun.eric.evaluationHAS.model.Customer;
import com.aubrun.eric.evaluationHAS.model.Establishment;
import com.aubrun.eric.evaluationHAS.model.EvaluationHASUser;
import com.aubrun.eric.evaluationHAS.model.Role;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class EvaluationHasUserService {
    private EvaluationHasUserRepository evaluationHasUserRepository;
    private CustomerRepository customerRepository;
    private EstablishmentRepository establishmentRepository;
    private RoleRepository roleRepository;

    public List<EvaluationHASUserDto> findAll(){
        return evaluationHasUserRepository.findAll().stream().map(EvaluationHASUserDtoMapper::toDto).collect(Collectors.toList());
    }

    public EvaluationHASUserDto findById(int id){
        return EvaluationHASUserDtoMapper.toDto(evaluationHasUserRepository.findById(id).orElseThrow(RuntimeException::new));
    }

    public void save(EvaluationHASUserDto newUser){
        EvaluationHASUser evaluationHASUser = new EvaluationHASUser();
        evaluationHASUser.setUsername(newUser.getUsername());
        evaluationHASUser.setDateBirthUser(newUser.getDateBirthUser());
        evaluationHASUser.setSocialSecurityNumberUser(newUser.getSocialSecurityNumberUser());
        evaluationHASUser.setPhoneUser(newUser.getPhoneUser());
        evaluationHASUser.setEntryDateUser(newUser.getEntryDateUser());
        evaluationHASUser.setAgeUser(newUser.getAgeUser());
        evaluationHASUser.setPassword(newUser.getPassword());
        evaluationHASUser.setEmail(newUser.getEmail());
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void delete(int id){
        evaluationHasUserRepository.deleteById(id);
    }

    public void updateUser(int evaluationHasUserId, LocalDateTime releaseDateUser){
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findById(evaluationHasUserId).orElseThrow(RuntimeException::new);
        evaluationHASUser.setReleaseDateUser(releaseDateUser);
        List<Customer> customers = evaluationHASUser.getCustomerList().stream().peek(c -> c.setEvaluationHASUser(null)).toList();
        evaluationHASUser.setCustomerList(new ArrayList<>());
        evaluationHasUserRepository.save(evaluationHASUser);
        customerRepository.saveAll(customers);
    }

    public void addEstablishment(int evaluationHasUserId, List<Integer> establishment){
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findById(evaluationHasUserId).orElseThrow(RuntimeException::new);
        List<Establishment> establishmentList = establishmentRepository.findAllById(establishment);
        evaluationHASUser.getEstablishments().addAll(establishmentList);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void addRole(int evaluationHasUserId, List<Integer> roles){
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findById(evaluationHasUserId).orElseThrow(RuntimeException::new);
        List<Role> roleList = roleRepository.findAllById(roles);
        evaluationHASUser.getRoles().addAll(roleList);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void deleteEstablishment(int evaluationHasUserId){
        evaluationHasUserRepository.delEstablishment(evaluationHasUserId);
    }

    public void deleteRole(int evaluationHasUserId){
        evaluationHasUserRepository.delRole(evaluationHasUserId);
    }
}
