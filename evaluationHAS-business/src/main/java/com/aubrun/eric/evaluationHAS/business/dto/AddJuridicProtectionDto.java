package com.aubrun.eric.evaluationHAS.business.dto;

import com.aubrun.eric.evaluationHAS.model.annotations.ExcludeClassFromJacocoGeneratedReport;

import java.util.Collections;
import java.util.List;

@ExcludeClassFromJacocoGeneratedReport
public class AddJuridicProtectionDto {

    private List<Integer> juridicProtection;

    public AddJuridicProtectionDto(int juridicProtection) {
        this.juridicProtection = Collections.singletonList(juridicProtection);
    }

    public List<Integer> getJuridicProtection() {
        return juridicProtection;
    }

    public void setJuridicProtection(List<Integer> juridicProtection) {
        this.juridicProtection = juridicProtection;
    }
}
