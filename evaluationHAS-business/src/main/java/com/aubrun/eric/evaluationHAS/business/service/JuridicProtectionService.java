package com.aubrun.eric.evaluationHAS.business.service;

import com.aubrun.eric.evaluationHAS.business.dto.JuridicProtectionDto;
import com.aubrun.eric.evaluationHAS.consumer.repository.EvaluationHasUserRepository;
import com.aubrun.eric.evaluationHAS.consumer.repository.JuridicProtectionRepository;
import com.aubrun.eric.evaluationHAS.model.EvaluationHASUser;
import com.aubrun.eric.evaluationHAS.model.JuridicProtection;
import com.aubrun.eric.evaluationHAS.business.mapper.JuridicProtectionDtoMapper;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class JuridicProtectionService {

    private JuridicProtectionRepository juridicProtectionRepository;

    private EvaluationHasUserRepository evaluationHasUserRepository;

    public List<JuridicProtectionDto> findAll() {
        return juridicProtectionRepository.findAll().stream().map(JuridicProtectionDtoMapper::toDto).collect(Collectors.toList());
    }

    public JuridicProtectionDto findById(int juridicProtectionId) {
        return JuridicProtectionDtoMapper.toDto(juridicProtectionRepository.findById(juridicProtectionId).orElseThrow(RuntimeException::new));
    }

    public void save(String username, JuridicProtectionDto juridicProtectionDto) {
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findByUsername(username).orElseThrow(RuntimeException::new);
        JuridicProtection juridicProtection = new JuridicProtection();
        juridicProtection.setJuridicProtectionName(juridicProtectionDto.getJuridicProtectionName());
        juridicProtectionRepository.save(juridicProtection);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void update(JuridicProtectionDto juridicProtectionDto, int juridicProtectionDtoId) {
        JuridicProtection juridicProtection = juridicProtectionRepository.findById(juridicProtectionDtoId).orElseThrow(RuntimeException::new);
        juridicProtection.setJuridicProtectionName(juridicProtectionDto.getJuridicProtectionName());
        juridicProtectionRepository.save(juridicProtection);
    }

    public void deleteById(int juridicProtectionId){
        juridicProtectionRepository.deleteById(juridicProtectionId);
    }
}
