package com.aubrun.eric.evaluationHAS.business.mapper;

import com.aubrun.eric.evaluationHAS.business.dto.PersonThemesDto;
import com.aubrun.eric.evaluationHAS.model.PersonThemes;

public class PersonThemesDtoMapper {

    static public PersonThemesDto toDto(PersonThemes personThemes) {

        PersonThemesDto dto = new PersonThemesDto();
        dto.setPersonThemesId(personThemes.getPersonThemesId());
        dto.setCriteriaPersonName(personThemes.getCriteriaPersonName());
        dto.setPersonThemesType(personThemes.getPersonThemesType());
        return dto;
    }

    static public PersonThemes toEntity(PersonThemesDto personThemesDto) {

        PersonThemes entity = new PersonThemes();
        entity.setPersonThemesId(personThemesDto.getPersonThemesId());
        entity.setCriteriaPersonName(personThemesDto.getCriteriaPersonName());
        entity.setPersonThemesType(personThemesDto.getPersonThemesType());
        return entity;
    }
}
