package com.aubrun.eric.evaluationHAS.business.service;

import com.aubrun.eric.evaluationHAS.model.EvaluationHASUser;
import com.aubrun.eric.evaluationHAS.consumer.repository.EvaluationHasUserRepository;
import com.aubrun.eric.evaluationHAS.model.annotations.ExcludeClassFromJacocoGeneratedReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@ExcludeClassFromJacocoGeneratedReport
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    EvaluationHasUserRepository evaluationHasUserRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        EvaluationHASUser user = evaluationHasUserRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
        return UserDetailsImpl.build(user);
    }
}
