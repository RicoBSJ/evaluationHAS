package com.aubrun.eric.evaluationHAS.business.dto;

import com.aubrun.eric.evaluationHAS.model.EESMSThemes;

public class ESMSThemesDto {

    private Integer esmsThemesId;
    private String criteriaESMSName;
    private EESMSThemes esmsThemesType;

    public Integer getEsmsThemesId() {
        return esmsThemesId;
    }

    public void setEsmsThemesId(Integer esmsThemesId) {
        this.esmsThemesId = esmsThemesId;
    }

    public String getCriteriaESMSName() {
        return criteriaESMSName;
    }

    public void setCriteriaESMSName(String criteriaESMSName) {
        this.criteriaESMSName = criteriaESMSName;
    }

    public EESMSThemes getEsmsThemesType() {
        return esmsThemesType;
    }

    public void setEsmsThemesType(EESMSThemes esmsThemesType) {
        this.esmsThemesType = esmsThemesType;
    }
}
