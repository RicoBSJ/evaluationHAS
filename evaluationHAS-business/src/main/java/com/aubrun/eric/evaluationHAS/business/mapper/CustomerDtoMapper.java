package com.aubrun.eric.evaluationHAS.business.mapper;

import com.aubrun.eric.evaluationHAS.business.dto.*;
import com.aubrun.eric.evaluationHAS.model.Customer;

import java.util.stream.Collectors;

public class CustomerDtoMapper {

    static public CustomerDto toDto(Customer customer) {

        CustomerDto dto = new CustomerDto();
        dto.setCustomerId(customer.getCustomerId());
        dto.setCustomerFirstName(customer.getCustomerFirstName());
        dto.setCustomerLastName(customer.getCustomerLastName());
        dto.setEvaluationHASUser(customer.getEvaluationHASUser() == null ? "" :customer.getEvaluationHASUser().getUsername()+" "+customer.getEvaluationHASUser().getUsername());
        dto.setAge(customer.getAge());
        dto.setDateBirth(customer.getDateBirth());
        dto.setEntryDate(customer.getEntryDate());
        dto.setReleaseDate(customer.getReleaseDate());
        dto.setSocialSecurityNumber(customer.getSocialSecurityNumber());
        dto.setMutualName(customer.getMutualName());
        dto.setEstablishmentDtoSet(customer.getEstablishmentList().stream().map(EstablishmentDtoMapper::toDto).collect(Collectors.toSet()));
        dto.setJuridicProtectionDtoSet(customer.getJuridicProtectionList().stream().map(JuridicProtectionDtoMapper::toDto).collect(Collectors.toSet()));
        dto.setPersonThemesDtoList(customer.getPersonThemesList().stream().map(PersonThemesDtoMapper::toDto).collect(Collectors.toList()));
        dto.setProfessionalsThemesDtoList(customer.getProfessionalsThemesList().stream().map(ProfessionalsThemesDtoMapper::toDto).collect(Collectors.toList()));
        dto.setEsmsThemesDtoList(customer.getEsmsThemesList().stream().map(ESMSThemesDtoMapper::toDto).collect(Collectors.toList()));
        return dto;
    }

    /*static public Customer toEntity(CustomerDto customerDto) {

        Customer entity = new Customer();
        entity.setCustomerId(customerDto.getCustomerId());
        entity.setEvaluationHASUser(EvaluationHASUserDtoMapper.toEntity(customerDto.getEvaluationHASUserDto()));
        entity.setCustomerFirstName(customerDto.getCustomerFirstName());
        entity.setCustomerLastName(customerDto.getCustomerLastName());
        entity.setAge(customerDto.getAge());
        entity.setDateBirth(customerDto.getDateBirth());
        entity.setEntryDate(customerDto.getEntryDate());
        entity.setReleaseDate(customerDto.getReleaseDate());
        entity.setSocialSecurityNumber(customerDto.getSocialSecurityNumber());
        entity.setMutualName(customerDto.getMutualName());
        entity.setEstablishmentList(customerDto.getEstablishmentDtoSet().stream().map(EstablishmentDtoMapper::toEntity).collect(Collectors.toSet()));
        entity.setJuridicProtectionList(customerDto.getJuridicProtectionDtoSet().stream().map(JuridictionDtoMapper::toEntity).collect(Collectors.toSet()));
        entity.setPersonThemesList(customerDto.getPersonThemesDtoList().stream().map(PersonThemesDtoMapper::toEntity).collect(Collectors.toList()));
        entity.setProfessionalsThemesList(customerDto.getProfessionalsThemesDtoList().stream().map(ProfessionalsThemesDtoMapper::toEntity).collect(Collectors.toList()));
        entity.setEsmsThemesList(customerDto.getEsmsThemesDtoList().stream().map(ESMSThemesDtoMapper::toEntity).collect(Collectors.toList()));
        return entity;
    }*/
}
