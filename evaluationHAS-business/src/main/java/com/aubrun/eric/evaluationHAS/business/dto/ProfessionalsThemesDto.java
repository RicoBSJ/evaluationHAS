package com.aubrun.eric.evaluationHAS.business.dto;

import com.aubrun.eric.evaluationHAS.model.EProfessionalsThemes;

public class ProfessionalsThemesDto {

    private Integer professionalsThemesId;
    private String criteriaProfessionalsName;
    private EProfessionalsThemes professionalsThemesType;

    public Integer getProfessionalsThemesId() {
        return professionalsThemesId;
    }

    public void setProfessionalsThemesId(Integer professionalsThemesId) {
        this.professionalsThemesId = professionalsThemesId;
    }

    public String getCriteriaProfessionalsName() {
        return criteriaProfessionalsName;
    }

    public void setCriteriaProfessionalsName(String criteriaProfessionalsName) {
        this.criteriaProfessionalsName = criteriaProfessionalsName;
    }

    public EProfessionalsThemes getProfessionalsThemesType() {
        return professionalsThemesType;
    }

    public void setProfessionalsThemesType(EProfessionalsThemes professionalsThemesType) {
        this.professionalsThemesType = professionalsThemesType;
    }
}
