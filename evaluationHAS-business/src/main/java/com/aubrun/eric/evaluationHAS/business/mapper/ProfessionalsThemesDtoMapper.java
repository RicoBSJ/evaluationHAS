package com.aubrun.eric.evaluationHAS.business.mapper;

import com.aubrun.eric.evaluationHAS.business.dto.ProfessionalsThemesDto;
import com.aubrun.eric.evaluationHAS.model.ProfessionalsThemes;

public class ProfessionalsThemesDtoMapper {

    static public ProfessionalsThemesDto toDto(ProfessionalsThemes professionalsThemes){

        ProfessionalsThemesDto dto = new ProfessionalsThemesDto();
        dto.setProfessionalsThemesId(professionalsThemes.getProfessionalsThemesId());
        dto.setCriteriaProfessionalsName(professionalsThemes.getCriteriaProfessionalsName());
        dto.setProfessionalsThemesType(professionalsThemes.getProfessionalsThemesType());
        return dto;
    }

    static public ProfessionalsThemes toEntity(ProfessionalsThemesDto professionalsThemesDto){

        ProfessionalsThemes entity = new ProfessionalsThemes();
        entity.setProfessionalsThemesId(professionalsThemesDto.getProfessionalsThemesId());
        entity.setCriteriaProfessionalsName(professionalsThemesDto.getCriteriaProfessionalsName());
        entity.setProfessionalsThemesType(professionalsThemesDto.getProfessionalsThemesType());
        return entity;
    }
}
