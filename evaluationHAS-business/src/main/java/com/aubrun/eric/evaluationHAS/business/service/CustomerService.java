package com.aubrun.eric.evaluationHAS.business.service;

import com.aubrun.eric.evaluationHAS.business.dto.CustomerDto;
import com.aubrun.eric.evaluationHAS.business.dto.SearchCustomerDto;
import com.aubrun.eric.evaluationHAS.business.mapper.CustomerDtoMapper;
import com.aubrun.eric.evaluationHAS.business.mapper.SearchCustomerDtoMapper;
import com.aubrun.eric.evaluationHAS.consumer.repository.*;
import com.aubrun.eric.evaluationHAS.model.*;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CustomerService {

    private CustomerRepository customerRepository;
    private EvaluationHasUserRepository evaluationHasUserRepository;
    private EstablishmentRepository establishmentRepository;
    private JuridicProtectionRepository juridicProtectionRepository;
    private SearchRepository searchRepository;


    public List<CustomerDto> findCustomersModerator() {
        return customerRepository.findAll().stream().map(CustomerDtoMapper::toDto).collect(Collectors.toList());
    }

    public List<CustomerDto> findAll(Principal principal) {
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findByUsername(principal.getName()).orElseThrow(RuntimeException::new);
        return customerRepository.findAllByEvaluationHASUser(evaluationHASUser).stream().map(CustomerDtoMapper::toDto).collect(Collectors.toList());
    }

    public void save(CustomerDto addCustomerDto, int nomenclatureUserId) {
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findById(nomenclatureUserId).orElseThrow(RuntimeException::new);
        Customer customer = new Customer();
        customer.setCustomerFirstName(addCustomerDto.getCustomerFirstName());
        customer.setCustomerLastName(addCustomerDto.getCustomerLastName());
        customer.setDateBirth(addCustomerDto.getDateBirth());
        customer.setSocialSecurityNumber(addCustomerDto.getSocialSecurityNumber());
        customer.setMutualName(addCustomerDto.getMutualName());
        customer.setEntryDate(addCustomerDto.getEntryDate());
        customer.setAge(addCustomerDto.getAge());
        customer.setEvaluationHASUser(evaluationHASUser);
        Customer customerSave = customerRepository.save(customer);
        evaluationHASUser.getCustomerList().add(customerSave);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void addEvaluationHasUser(int evaluationHasUserId, int customerId) {
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findById(evaluationHasUserId).orElseThrow(RuntimeException::new);
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        customer.setEvaluationHASUser(evaluationHASUser);
        evaluationHASUser.getCustomerList().add(customer);
        customerRepository.save(customer);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void addEstablishment(int customerId, List<Integer> establishment) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        List<Establishment> establishmentList = establishmentRepository.findAllById(establishment);
        customer.getEstablishmentList().addAll(establishmentList);
        customerRepository.save(customer);
    }

    public void addJuridicProtections(int customerId, List<Integer> juridicProtection) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        List<JuridicProtection> juridicProtectionList = juridicProtectionRepository.findAllById(juridicProtection);
        customer.getJuridicProtectionList().addAll(juridicProtectionList);
        customerRepository.save(customer);
    }

    public void updateCustomer(int customerId, LocalDateTime releaseDate) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        customer.setReleaseDate(releaseDate);
        if (customer.getEvaluationHASUser() != null){
            customer.getEvaluationHASUser().getCustomerList().remove(customer);
        }
        customer.setEvaluationHASUser(null);
        customerRepository.save(customer);
    }

    public void deleteByJuridicProtections(int customerId) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        customerRepository.deleteByJuridicProtectionList(customer.getCustomerId());
    }

    public void deleteByEstablishments(int customerId) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(RuntimeException::new);
        customerRepository.deleteByEstablishmentList(customer.getCustomerId());
    }

    public CustomerDto findById(Integer id) {
        return CustomerDtoMapper.toDto(customerRepository.findById(id).orElseThrow(RuntimeException::new));
    }

    public List<CustomerDto> searchCustomer(SearchCustomerDto searchCustomerDto){
        SearchCustomer searchCustomer = SearchCustomerDtoMapper.toEntity(searchCustomerDto);
        return searchRepository.findAllByNameAndDateBirthAndEntryDateAndAge(searchCustomer).stream().map(CustomerDtoMapper::toDto).collect(Collectors.toList());
    }
}