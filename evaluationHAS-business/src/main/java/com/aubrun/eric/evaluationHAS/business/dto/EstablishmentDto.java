package com.aubrun.eric.evaluationHAS.business.dto;

import com.aubrun.eric.evaluationHAS.model.EEstablishment;

public class EstablishmentDto {

    private Integer establishmentId;
    private EEstablishment establishmentName;

    public Integer getEstablishmentId() {
        return establishmentId;
    }

    public void setEstablishmentId(Integer establishmentId) {
        this.establishmentId = establishmentId;
    }

    public EEstablishment getEstablishmentName() {
        return establishmentName;
    }

    public void setEstablishmentName(EEstablishment establishmentName) {
        this.establishmentName = establishmentName;
    }
}
