package com.aubrun.eric.evaluationHAS.business.service;

import com.aubrun.eric.evaluationHAS.business.dto.EstablishmentDto;
import com.aubrun.eric.evaluationHAS.consumer.repository.EstablishmentRepository;
import com.aubrun.eric.evaluationHAS.consumer.repository.EvaluationHasUserRepository;
import com.aubrun.eric.evaluationHAS.model.Establishment;
import com.aubrun.eric.evaluationHAS.model.EvaluationHASUser;
import com.aubrun.eric.evaluationHAS.business.mapper.EstablishmentDtoMapper;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class EstablishmentService {

    private EstablishmentRepository establishmentRepository;

    private EvaluationHasUserRepository evaluationHasUserRepository;

    public List<EstablishmentDto> findAll() {
        return establishmentRepository.findAll().stream().map(EstablishmentDtoMapper::toDto).collect(Collectors.toList());
    }

    public EstablishmentDto findById(int establishmentId) {
        return EstablishmentDtoMapper.toDto(establishmentRepository.findById(establishmentId).orElseThrow(RuntimeException::new));
    }

    public void save(String username, EstablishmentDto establishmentDto) {
        EvaluationHASUser evaluationHASUser = evaluationHasUserRepository.findByUsername(username).orElseThrow(RuntimeException::new);
        Establishment establishment = new Establishment();
        establishment.setEstablishmentName(establishmentDto.getEstablishmentName());
        establishmentRepository.save(establishment);
        evaluationHasUserRepository.save(evaluationHASUser);
    }

    public void update(EstablishmentDto establishmentDto, int establishmentDtoId) {
        Establishment establishment = establishmentRepository.findById(establishmentDtoId).orElseThrow(RuntimeException::new);
        establishment.setEstablishmentName(establishmentDto.getEstablishmentName());
        establishmentRepository.save(establishment);
    }

    public void deleteById(int establishmentDtoId){
        establishmentRepository.deleteById(establishmentDtoId);
    }

}
