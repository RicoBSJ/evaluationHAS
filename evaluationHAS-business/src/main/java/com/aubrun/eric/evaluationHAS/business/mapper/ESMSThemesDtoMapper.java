package com.aubrun.eric.evaluationHAS.business.mapper;

import com.aubrun.eric.evaluationHAS.business.dto.ESMSThemesDto;
import com.aubrun.eric.evaluationHAS.model.ESMSThemes;

public class ESMSThemesDtoMapper {

    static public ESMSThemesDto toDto(ESMSThemes esmsThemes){

        ESMSThemesDto dto = new ESMSThemesDto();
        dto.setEsmsThemesId(esmsThemes.getEsmsThemesId());
        dto.setCriteriaESMSName(esmsThemes.getCriteriaESMSName());
        dto.setEsmsThemesType(esmsThemes.getEsmsThemesType());
        return dto;
    }

    static public ESMSThemes toEntity(ESMSThemesDto esmsThemesDto){

        ESMSThemes entity = new ESMSThemes();
        entity.setEsmsThemesId(esmsThemesDto.getEsmsThemesId());
        entity.setCriteriaESMSName(esmsThemesDto.getCriteriaESMSName());
        entity.setEsmsThemesType(esmsThemesDto.getEsmsThemesType());
        return entity;
    }
}
