package com.aubrun.eric.evaluationHAS.consumer.repository;

import com.aubrun.eric.evaluationHAS.model.EvaluationHASUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EvaluationHasUserRepository extends JpaRepository<EvaluationHASUser, Integer> {

    Optional<EvaluationHASUser> findByUsername(String username);

    Boolean existsByUsername(String username);
    boolean existsByEmail (String email);

    @Modifying
    @Query(value = "DELETE FROM user_establishments WHERE user_establishment_id=:evaluationHasUserId",nativeQuery = true)
    void delEstablishment(Integer evaluationHasUserId);

    @Modifying
    @Query(value = "DELETE FROM user_roles WHERE user_roles_id=:evaluationHasUserId",nativeQuery = true)
    void delRole(Integer evaluationHasUserId);
}
