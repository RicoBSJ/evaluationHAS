package com.aubrun.eric.evaluationHAS.consumer.repository;

import com.aubrun.eric.evaluationHAS.model.Customer;
import com.aubrun.eric.evaluationHAS.model.EvaluationHASUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    @Modifying
    @Query(value = "DELETE FROM juridic_protection_list WHERE customer_id=:customerId",nativeQuery = true)
    void deleteByJuridicProtectionList(Integer customerId);

    @Modifying
    @Query(value = "DELETE FROM establishment_list WHERE customer_id=:customerId",nativeQuery = true)
    void deleteByEstablishmentList(Integer customerId);

    @Modifying
    @Query(value = "DELETE FROM person_themes_list WHERE customer_id=:customerId",nativeQuery = true)
    void deleteByPersonThemesList(Integer customerId);

    @Modifying
    @Query(value = "DELETE FROM professionals_themes_list WHERE customer_id=:customerId",nativeQuery = true)
    void deleteByProfessionalsThemesList(Integer customerId);

    @Modifying
    @Query(value = "DELETE FROM esms_themes_list WHERE customer_id=:customerId",nativeQuery = true)
    void deleteByEsmsThemesList(Integer customerId);

    @Query(value = "SELECT c FROM Customer c WHERE c.evaluationHASUser=:evaluationHASUser")
    List<Customer> findAllByEvaluationHASUser (EvaluationHASUser evaluationHASUser);
}
