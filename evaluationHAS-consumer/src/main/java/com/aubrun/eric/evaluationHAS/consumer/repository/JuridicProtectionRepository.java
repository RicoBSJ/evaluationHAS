package com.aubrun.eric.evaluationHAS.consumer.repository;

import com.aubrun.eric.evaluationHAS.model.JuridicProtection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JuridicProtectionRepository extends JpaRepository<JuridicProtection, Integer> {

}
