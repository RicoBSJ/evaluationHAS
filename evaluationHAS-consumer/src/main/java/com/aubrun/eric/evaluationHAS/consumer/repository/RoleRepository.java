package com.aubrun.eric.evaluationHAS.consumer.repository;

import com.aubrun.eric.evaluationHAS.model.ERole;
import com.aubrun.eric.evaluationHAS.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

    Optional<Role> findByRoleName(ERole roleName);
}
