package com.aubrun.eric.evaluationHAS.model;

public enum EJuridicProtection {

    TRUSTEESHIP,
    TUTORSHIP,
    FAMILY_GUARDIANSHIP,
    NOT_CONCERNED
}
