package com.aubrun.eric.evaluationHAS.model;

import jakarta.persistence.*;

@Entity
@SequenceGenerator(name = "esms_themes_id_generator", sequenceName = "esms_themes_id_seq", allocationSize = 1)
@Table(name = "ESMS_THEMES")
public class ESMSThemes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "esms_themes_id_generator")
    @Column(name = "esms_themes_id")
    private Integer esmsThemesId;
    @Column(name = "criteria_esms_name")
    private String criteriaESMSName;
    @Enumerated(EnumType.STRING)
    @Column(name = "esms_themes_type")
    private EESMSThemes esmsThemesType;

    public ESMSThemes() {
    }

    public Integer getEsmsThemesId() {
        return esmsThemesId;
    }

    public void setEsmsThemesId(Integer esmsThemesId) {
        this.esmsThemesId = esmsThemesId;
    }

    public String getCriteriaESMSName() {
        return criteriaESMSName;
    }

    public void setCriteriaESMSName(String criteriaESMSName) {
        this.criteriaESMSName = criteriaESMSName;
    }

    public EESMSThemes getEsmsThemesType() {
        return esmsThemesType;
    }

    public void setEsmsThemesType(EESMSThemes esmsThemesType) {
        this.esmsThemesType = esmsThemesType;
    }

    @Override
    public String toString() {
        return "ESMSThemes{" +
                "esmsThemesId=" + esmsThemesId +
                ", criteriaESMSName='" + criteriaESMSName + '\'' +
                ", esmsThemesType=" + esmsThemesType +
                '}';
    }
}
