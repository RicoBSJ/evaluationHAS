package com.aubrun.eric.evaluationHAS.model;

public enum ERole {

    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
