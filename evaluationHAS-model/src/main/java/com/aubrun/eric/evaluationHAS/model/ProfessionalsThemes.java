package com.aubrun.eric.evaluationHAS.model;

import jakarta.persistence.*;

@Entity
@SequenceGenerator(name = "professionals_themes_id_generator", sequenceName = "professionals_themes_id_seq", allocationSize = 1)
@Table(name = "PROFESSIONALS_THEMES")
public class ProfessionalsThemes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "professionals_themes_id_generator")
    @Column(name = "professionals_themes_id")
    private Integer professionalsThemesId;
    @Column(name = "criteria_professionals_name")
    private String criteriaProfessionalsName;
    @Enumerated(EnumType.STRING)
    @Column(name = "professionals_themes_type")
    private EProfessionalsThemes professionalsThemesType;

    public ProfessionalsThemes() {
    }

    public Integer getProfessionalsThemesId() {
        return professionalsThemesId;
    }

    public void setProfessionalsThemesId(Integer professionalsThemesId) {
        this.professionalsThemesId = professionalsThemesId;
    }

    public String getCriteriaProfessionalsName() {
        return criteriaProfessionalsName;
    }

    public void setCriteriaProfessionalsName(String criteriaProfessionalsName) {
        this.criteriaProfessionalsName = criteriaProfessionalsName;
    }

    public EProfessionalsThemes getProfessionalsThemesType() {
        return professionalsThemesType;
    }

    public void setProfessionalsThemesType(EProfessionalsThemes professionalsThemesType) {
        this.professionalsThemesType = professionalsThemesType;
    }

    @Override
    public String toString() {
        return "ProfessionalsThemes{" +
                "professionalsThemesId=" + professionalsThemesId +
                ", criteriaProfessionalsName='" + criteriaProfessionalsName + '\'' +
                ", professionalsThemesType=" + professionalsThemesType +
                '}';
    }
}
