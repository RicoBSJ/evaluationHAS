package com.aubrun.eric.evaluationHAS.model;

import jakarta.persistence.*;

@Entity
@SequenceGenerator(name = "establishment_id_generator", sequenceName = "establishment_id_seq", allocationSize = 1)
@Table(name = "ESTABLISHMENT")
public class Establishment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "establishment_id_generator")
    @Column(name = "establishment_id")
    private Integer establishmentId;
    @Enumerated(EnumType.STRING)
    @Column(name = "establishment_name")
    private EEstablishment establishmentName;

    public Establishment() {
    }

    public Integer getEstablishmentId() {
        return establishmentId;
    }

    public void setEstablishmentId(Integer establishmentId) {
        this.establishmentId = establishmentId;
    }

    public EEstablishment getEstablishmentName() {
        return establishmentName;
    }

    public void setEstablishmentName(EEstablishment establishmentName) {
        this.establishmentName = establishmentName;
    }

    @Override
    public String toString() {
        return "Establishment{" +
                "establishmentId=" + establishmentId +
                ", establishmentName=" + establishmentName +
                '}';
    }
}
