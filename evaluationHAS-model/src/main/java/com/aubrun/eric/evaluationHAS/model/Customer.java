package com.aubrun.eric.evaluationHAS.model;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Entity
@SequenceGenerator(name = "customer_id_generator", sequenceName = "customer_id_seq", allocationSize = 1)
@Table(name = "CUSTOMER")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "customer_id_generator")
    @Column(name = "customer_id")
    private Integer customerId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "evaluation_has_user")
    private EvaluationHASUser evaluationHASUser;
    @Column(name = "customer_first_name")
    private String customerFirstName;
    @Column(name = "customer_last_name")
    private String customerLastName;
    @Column(name = "date_birth")
    private LocalDate dateBirth;
    @Column(name = "social_security_number")
    private Long socialSecurityNumber;
    @Column(name = "mutual_name")
    private String mutualName;
    @Column(name = "entry_date")
    private LocalDateTime entryDate;
    @Column(name = "release_date")
    private LocalDateTime releaseDate;
    @Column(name = "age_customer")
    private Integer age;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "juridic_protection_list",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "juridic_protection_id"))
    private Set<JuridicProtection> juridicProtectionList;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "establishment_list",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "establishment_id"))
    private Set<Establishment> establishmentList;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "person_themes_list",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "person_themes_id"))
    private List<PersonThemes> personThemesList;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "professionals_themes_list",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "professionals_themes_id"))
    private List<ProfessionalsThemes> professionalsThemesList;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "esms_themes_list",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "esms_themes_id"))
    private List<ESMSThemes> esmsThemesList;

    public Customer() {
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public EvaluationHASUser getEvaluationHASUser() {
        return evaluationHASUser;
    }

    public void setEvaluationHASUser(EvaluationHASUser evaluationHASUser) {
        this.evaluationHASUser = evaluationHASUser;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public LocalDate getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(LocalDate dateBirth) {
        this.dateBirth = dateBirth;
    }

    public Long getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(Long socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getMutualName() {
        return mutualName;
    }

    public void setMutualName(String mutualName) {
        this.mutualName = mutualName;
    }

    public LocalDateTime getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(LocalDateTime entryDate) {
        this.entryDate = entryDate;
    }

    public LocalDateTime getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDateTime releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Set<JuridicProtection> getJuridicProtectionList() {
        return juridicProtectionList;
    }

    public void setJuridicProtectionList(Set<JuridicProtection> juridicProtectionList) {
        this.juridicProtectionList = juridicProtectionList;
    }

    public Set<Establishment> getEstablishmentList() {
        return establishmentList;
    }

    public void setEstablishmentList(Set<Establishment> establishmentList) {
        this.establishmentList = establishmentList;
    }

    public List<PersonThemes> getPersonThemesList() {
        return personThemesList;
    }

    public void setPersonThemesList(List<PersonThemes> personThemesList) {
        this.personThemesList = personThemesList;
    }

    public List<ProfessionalsThemes> getProfessionalsThemesList() {
        return professionalsThemesList;
    }

    public void setProfessionalsThemesList(List<ProfessionalsThemes> professionalsThemesList) {
        this.professionalsThemesList = professionalsThemesList;
    }

    public List<ESMSThemes> getEsmsThemesList() {
        return esmsThemesList;
    }

    public void setEsmsThemesList(List<ESMSThemes> esmsThemesList) {
        this.esmsThemesList = esmsThemesList;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerId=" + customerId +
                ", evaluationHASUser=" + evaluationHASUser +
                ", customerFirstName='" + customerFirstName + '\'' +
                ", customerLastName='" + customerLastName + '\'' +
                ", dateBirth=" + dateBirth +
                ", socialSecurityNumber=" + socialSecurityNumber +
                ", mutualName='" + mutualName + '\'' +
                ", entryDate=" + entryDate +
                ", releaseDate=" + releaseDate +
                ", age=" + age +
                ", juridicProtectionList=" + juridicProtectionList +
                ", establishmentList=" + establishmentList +
                ", personThemesList=" + personThemesList +
                ", professionalsThemesList=" + professionalsThemesList +
                ", esmsThemesList=" + esmsThemesList +
                '}';
    }
}
