package com.aubrun.eric.evaluationHAS.model;

public enum EEstablishment {

    FOYER_HEB,
    FOYER_VIE,
    OTHER
}
