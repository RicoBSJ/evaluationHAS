package com.aubrun.eric.evaluationHAS.model;

import jakarta.persistence.*;

@Entity
@SequenceGenerator(name = "person_themes_id_generator", sequenceName = "person_themes_id_seq", allocationSize = 1)
@Table(name = "PERSON_THEMES")
public class PersonThemes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "person_themes_id_generator")
    @Column(name = "person_themes_id")
    private Integer personThemesId;
    @Column(name = "criteria_person_name")
    private String criteriaPersonName;
    @Enumerated(EnumType.STRING)
    @Column(name = "person_themes_type")
    private EPersonThemes personThemesType;

    public PersonThemes() {
    }

    public Integer getPersonThemesId() {
        return personThemesId;
    }

    public void setPersonThemesId(Integer personThemesId) {
        this.personThemesId = personThemesId;
    }

    public String getCriteriaPersonName() {
        return criteriaPersonName;
    }

    public void setCriteriaPersonName(String criteriaPersonName) {
        this.criteriaPersonName = criteriaPersonName;
    }

    public EPersonThemes getPersonThemesType() {
        return personThemesType;
    }

    public void setPersonThemesType(EPersonThemes personThemesType) {
        this.personThemesType = personThemesType;
    }

    @Override
    public String toString() {
        return "PersonThemes{" +
                "personThemesId=" + personThemesId +
                ", criteriaPersonName='" + criteriaPersonName + '\'' +
                ", personThemesType=" + personThemesType +
                '}';
    }
}
