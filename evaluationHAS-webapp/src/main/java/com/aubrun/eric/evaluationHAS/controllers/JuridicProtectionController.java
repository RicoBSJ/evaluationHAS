package com.aubrun.eric.evaluationHAS.controllers;

import com.aubrun.eric.evaluationHAS.business.dto.JuridicProtectionDto;
import com.aubrun.eric.evaluationHAS.business.service.JuridicProtectionService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/juridicProtections")
public class JuridicProtectionController {

    private JuridicProtectionService juridicProtectionService;

    @GetMapping("/")
    public List<JuridicProtectionDto> getAllJuridicProtections() {
        return this.juridicProtectionService.findAll();
    }

    @GetMapping("/{id}")
    public JuridicProtectionDto getJuridicProtectionById(@PathVariable(value = "id") int juridicProtectionId) {
        return this.juridicProtectionService.findById(juridicProtectionId);
    }

    @PostMapping("/")
    public void createJuridicProtections(Principal principal, @RequestBody JuridicProtectionDto juridicProtectionDto) {
        juridicProtectionService.save(principal.getName(), juridicProtectionDto);
    }

    @PutMapping("/{id}")
    public void updateJuridicProtection(@RequestBody JuridicProtectionDto juridicProtectionDto, @PathVariable(value = "id") int juridicProtectionDtoId) {
        juridicProtectionService.update(juridicProtectionDto, juridicProtectionDtoId);
    }

    @DeleteMapping("/{id}")
    public void deleteJuridicProtection(@PathVariable(value = "id") int juridicProtectionId) {
        juridicProtectionService.deleteById(juridicProtectionId);
    }
}
