package com.aubrun.eric.evaluationHAS.controllers;

import com.aubrun.eric.evaluationHAS.business.dto.*;
import com.aubrun.eric.evaluationHAS.business.service.CustomerService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    private CustomerService customerService;

    @GetMapping("/moderatorAccess")
    public List<CustomerDto> getCustomersModerator() {
        return this.customerService.findCustomersModerator();
    }

    @GetMapping("/")
    public List<CustomerDto> getAllCustomers(Principal principal) {
        return this.customerService.findAll(principal);
    }

    @GetMapping("/{id}")
    public CustomerDto getCustomerById(@PathVariable(value = "id") int customerId) {
        return this.customerService.findById(customerId);
    }

    @PostMapping("/")
    public void createCustomer(@RequestBody CustomerDto customerDto) {
        customerService.save(customerDto, Integer.parseInt(customerDto.getEvaluationHASUser()));
    }

    @PutMapping("/addReleaseDate/{id}")
    public void updateCustomer(@RequestBody CustomerDto customerDto, @PathVariable(value = "id") int customerId) {
        customerService.updateCustomer(customerId, customerDto.getReleaseDate());
    }

    @PutMapping("/addEvaluationHasUser/{id}")
    public void addEvaluationHasUser(@RequestBody EvaluationHASUserDto evaluationHASUserDto, @PathVariable(value = "id") int customerId) {
        customerService.addEvaluationHasUser(evaluationHASUserDto.getEvaluationHasUserId(), customerId);
    }

    @PutMapping("/addEstablishmentCustomer/{id}")
    public void addEstablishment(@RequestBody AddEstablishmentsDto establishmentDto, @PathVariable(value = "id") int customerId) {
        customerService.addEstablishment(customerId, establishmentDto.getEstablishments());
    }

    @PutMapping("/addJuridicProtections/{id}")
    public void addJuridicProtections(@RequestBody AddJuridicProtectionDto juridicProtectionDto, @PathVariable(value = "id") int customerId) {
        customerService.addJuridicProtections(customerId, juridicProtectionDto.getJuridicProtection());
    }

    @PostMapping(value = "/search")
    private List<CustomerDto> search(@RequestBody SearchCustomerDto searchCustomerDto){
        return customerService.searchCustomer(searchCustomerDto);
    }

    @DeleteMapping("/delJuridicProtection/{id}")
    public void deleteByJuridicProtections(@PathVariable(value = "id") int customerId) {
        customerService.deleteByJuridicProtections(customerId);
    }

    @DeleteMapping("/delEstablishment/{id}")
    public void deleteByEstablishments(@PathVariable(value = "id") int customerId) {
        customerService.deleteByEstablishments(customerId);
    }

    /*
    @PutMapping("/addNeeds/{id}")
    public void addNeeds(@RequestBody AddNeedsDto addNeedsDto, @PathVariable(value = "id") int customerId) {
        customerService.addNeeds(customerId, addNeedsDto.getNeeds());
    }

    @PutMapping("/addDirectBenefits/{id}")
    public void addDirectBenefits(@RequestBody AddDirectBenefitsDto addDirectBenefitsDto, @PathVariable(value = "id") int customerId) {
        customerService.addDirectBenefits(customerId, addDirectBenefitsDto.getDirectBenefits());
    }

    @PutMapping("/addIndirectBenefits/{id}")
    public void addIndirectBenefits(@RequestBody AddIndirectBenefitsDto addIndirectBenefitsDto, @PathVariable(value = "id") int customerId) {
        customerService.addIndirectBenefits(customerId, addIndirectBenefitsDto.getIndirectBenefits());
    }

    @DeleteMapping("/delNeed/{id}")
    public void deleteByNeeds(@PathVariable(value = "id") int customerId) {
        customerService.deleteNeeds(customerId);
    }

    @DeleteMapping("/delDirectBenefits/{id}")
    public void deleteByDirectBenefits(@PathVariable(value = "id") int customerId){
        customerService.deleteDirectBenefits(customerId);
    }

    @DeleteMapping("/delIndirectBenefits/{id}")
    public void deleteByIndirectBenefits(@PathVariable("id") int customerId){
        customerService.deleteIndirectBenefits(customerId);
    }
     */
}